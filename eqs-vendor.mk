# Automatically generated file. DO NOT MODIFY
#
# This file is generated by device/motorola/eqs/setup-makefiles.sh

PRODUCT_SOONG_NAMESPACES += \
    vendor/motorola/eqs

PRODUCT_COPY_FILES += \
    vendor/motorola/eqs/proprietary/vendor/etc/permissions/com.motorola.camera3.content.ai.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/com.motorola.camera3.content.ai.xml \
    vendor/motorola/eqs/proprietary/vendor/etc/permissions/com.motorola.camera3.eqs.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/com.motorola.camera3.eqs.xml \
    vendor/motorola/eqs/proprietary/vendor/etc/permissions/com.motorola.camera3.lens.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/com.motorola.camera3.lens.xml \
    vendor/motorola/eqs/proprietary/product/etc/permissions/com.motorola.camera3.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.motorola.camera3.xml \
    vendor/motorola/eqs/proprietary/vendor/etc/permissions/com.motorola.camera3.xml:$(TARGET_COPY_OUT_VENDOR)/etc/permissions/com.motorola.camera3.xml \
    vendor/motorola/eqs/proprietary/product/etc/permissions/com.motorola.moto-uirefresh.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.motorola.moto-uirefresh.xml \
    vendor/motorola/eqs/proprietary/product/etc/permissions/deviceowner-configuration-com.motorola.camera3.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/deviceowner-configuration-com.motorola.camera3.xml \
    vendor/motorola/eqs/proprietary/product/etc/permissions/privapp-permissions-com.motorola.camera3.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-com.motorola.camera3.xml \
    vendor/motorola/eqs/proprietary/product/etc/sysconfig/hiddenapi-whitelist-com.motorola.camera3.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/hiddenapi-whitelist-com.motorola.camera3.xml \
    vendor/motorola/eqs/proprietary/system/etc/permissions/com.motorola.motosignature.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.motorola.motosignature.xml \
    vendor/motorola/eqs/proprietary/system/etc/permissions/moto-checkin.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/moto-checkin.xml \
    vendor/motorola/eqs/proprietary/system/etc/permissions/moto-core_services.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/moto-core_services.xml \
    vendor/motorola/eqs/proprietary/system/etc/permissions/moto-settings.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/moto-settings.xml \
    vendor/motorola/eqs/proprietary/system_ext/etc/permissions/com.android.hotwordenrollment.common.util.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.android.hotwordenrollment.common.util.xml \
    vendor/motorola/eqs/proprietary/vendor/etc/acdbdata/waipio_mtp/MTP_acdb_cal.acdb:$(TARGET_COPY_OUT_VENDOR)/etc/acdbdata/waipio_mtp/MTP_acdb_cal.acdb \
    vendor/motorola/eqs/proprietary/vendor/etc/acdbdata/waipio_mtp/MTP_workspaceFile.qwsp:$(TARGET_COPY_OUT_VENDOR)/etc/acdbdata/waipio_mtp/MTP_workspaceFile.qwsp \
    vendor/motorola/eqs/proprietary/vendor/etc/camera/aec_golden_tele.bin:$(TARGET_COPY_OUT_VENDOR)/etc/camera/aec_golden_tele.bin \
    vendor/motorola/eqs/proprietary/vendor/etc/camera/aec_golden_wide.bin:$(TARGET_COPY_OUT_VENDOR)/etc/camera/aec_golden_wide.bin \
    vendor/motorola/eqs/proprietary/vendor/etc/camera/dual_golden_tele.bin:$(TARGET_COPY_OUT_VENDOR)/etc/camera/dual_golden_tele.bin \
    vendor/motorola/eqs/proprietary/vendor/etc/camera/dual_golden_wide.bin:$(TARGET_COPY_OUT_VENDOR)/etc/camera/dual_golden_wide.bin \
    vendor/motorola/eqs/proprietary/vendor/etc/camera/mot_engine_config.bin:$(TARGET_COPY_OUT_VENDOR)/etc/camera/mot_engine_config.bin \
    vendor/motorola/eqs/proprietary/vendor/etc/camera/vidhance.lic:$(TARGET_COPY_OUT_VENDOR)/etc/camera/vidhance.lic \
    vendor/motorola/eqs/proprietary/vendor/etc/camera/vidhance_calibration:$(TARGET_COPY_OUT_VENDOR)/etc/camera/vidhance_calibration \
    vendor/motorola/eqs/proprietary/vendor/etc/display/qdcm_calib_data_mipi_mot_cmd_csot_1080p_dsc_667.json:$(TARGET_COPY_OUT_VENDOR)/etc/display/qdcm_calib_data_mipi_mot_cmd_csot_1080p_dsc_667.json \
    vendor/motorola/eqs/proprietary/vendor/etc/display/qdcm_calib_data_mipi_mot_cmd_csot_1080p_dsc_667_c6.json:$(TARGET_COPY_OUT_VENDOR)/etc/display/qdcm_calib_data_mipi_mot_cmd_csot_1080p_dsc_667_c6.json \
    vendor/motorola/eqs/proprietary/vendor/etc/display/qdcm_calib_data_mipi_mot_cmd_csot_ili_1080p_dsc_667.json:$(TARGET_COPY_OUT_VENDOR)/etc/display/qdcm_calib_data_mipi_mot_cmd_csot_ili_1080p_dsc_667.json \
    vendor/motorola/eqs/proprietary/vendor/etc/display/qdcm_calib_data_mipi_mot_cmd_tianma_1080p_667.json:$(TARGET_COPY_OUT_VENDOR)/etc/display/qdcm_calib_data_mipi_mot_cmd_tianma_1080p_667.json \
    vendor/motorola/eqs/proprietary/vendor/etc/display/qdcm_calib_data_mipi_mot_video_dummy_qhd.json:$(TARGET_COPY_OUT_VENDOR)/etc/display/qdcm_calib_data_mipi_mot_video_dummy_qhd.json \
    vendor/motorola/eqs/proprietary/vendor/etc/display/qdcm_calib_data_nt36672e_lcd_video_mode_dsi_novatek_panel_with_DSC.json:$(TARGET_COPY_OUT_VENDOR)/etc/display/qdcm_calib_data_nt36672e_lcd_video_mode_dsi_novatek_panel_with_DSC.json \
    vendor/motorola/eqs/proprietary/vendor/etc/display/qdcm_calib_data_nt36672e_lcd_video_mode_dsi_novatek_panel_without_DSC.json:$(TARGET_COPY_OUT_VENDOR)/etc/display/qdcm_calib_data_nt36672e_lcd_video_mode_dsi_novatek_panel_without_DSC.json \
    vendor/motorola/eqs/proprietary/vendor/etc/eva/facedetection/model3.dat:$(TARGET_COPY_OUT_VENDOR)/etc/eva/facedetection/model3.dat \
    vendor/motorola/eqs/proprietary/vendor/etc/face3d/qcnn_concat_file_model-0324_2_encrypted:$(TARGET_COPY_OUT_VENDOR)/etc/face3d/qcnn_concat_file_model-0324_2_encrypted \
    vendor/motorola/eqs/proprietary/vendor/etc/init/android.hardware.biometrics.fingerprint@2.1-service-ets.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/android.hardware.biometrics.fingerprint@2.1-service-ets.rc \
    vendor/motorola/eqs/proprietary/vendor/etc/init/android.hardware.nfc@1.2-service.st-moto.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/android.hardware.nfc@1.2-service.st-moto.rc \
    vendor/motorola/eqs/proprietary/vendor/etc/init/android.hardware.secure_element@1.2-service-gto-moto.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/android.hardware.secure_element@1.2-service-gto-moto.rc \
    vendor/motorola/eqs/proprietary/vendor/etc/init/motorola.hardware.sensorext.service.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/motorola.hardware.sensorext.service.rc \
    vendor/motorola/eqs/proprietary/vendor/etc/init/vendor.qti.camera.provider@2.7-service_64.rc:$(TARGET_COPY_OUT_VENDOR)/etc/init/vendor.qti.camera.provider@2.7-service_64.rc \
    vendor/motorola/eqs/proprietary/vendor/etc/jiigan/jiigan_model.data:$(TARGET_COPY_OUT_VENDOR)/etc/jiigan/jiigan_model.data \
    vendor/motorola/eqs/proprietary/vendor/etc/libnfc-hal-st.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-hal-st.conf \
    vendor/motorola/eqs/proprietary/vendor/etc/libnfc-hal-st54j.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-hal-st54j.conf \
    vendor/motorola/eqs/proprietary/vendor/etc/libnfc-hal-st54uicc.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-hal-st54uicc.conf \
    vendor/motorola/eqs/proprietary/vendor/etc/libnfc-nci-st.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-nci-st.conf \
    vendor/motorola/eqs/proprietary/vendor/etc/libse-gto-hal.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libse-gto-hal.conf \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/als_comp_config.xml:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/als_comp_config.xml \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/als_comp_tf_csotc4.config:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/als_comp_tf_csotc4.config \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/als_comp_tf_csotc6.config:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/als_comp_tf_csotc6.config \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/als_comp_tf_tianma.config:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/als_comp_tf_tianma.config \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/icm4x6xx_0.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/icm4x6xx_0.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/mot_camgest.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/mot_camgest.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/mot_chopchop.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/mot_chopchop.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/mot_drop.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/mot_drop.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/mot_ftm.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/mot_ftm.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/mot_ltv.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/mot_ltv.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/mot_tap.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/mot_tap.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/mot_vsync_psd.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/mot_vsync_psd.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/mxg4300_0.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/mxg4300_0.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/qmc6308_0.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/qmc6308_0.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/qsh_camera.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/qsh_camera.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_amd.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_amd.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_amd_sw_disabled.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_amd_sw_disabled.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_amd_sw_enabled.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_amd_sw_enabled.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_aont.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_aont.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_basic_gestures.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_basic_gestures.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_bring_to_ear.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_bring_to_ear.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_ccd_v1_0_amd.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_ccd_v1_0_amd.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_ccd_v1_0_ttw.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_ccd_v1_0_ttw.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_ccd_v2_0_walk.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_ccd_v2_0_walk.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_ccd_v3_0_walk.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_ccd_v3_0_walk.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_ccd_v3_1_walk.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_ccd_v3_1_walk.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_ccd_v4_0_sensors.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_ccd_v4_0_sensors.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_ccd_v4_0_te_cd_regs.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_ccd_v4_0_te_cd_regs.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_cm.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_cm.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_dae.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_dae.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_device_orient.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_device_orient.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_diag_filter.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_diag_filter.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_direct_channel.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_direct_channel.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_distance_bound.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_distance_bound.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_dpc.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_dpc.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_facing.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_facing.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_fmv.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_fmv.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_fmv_legacy.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_fmv_legacy.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_geomag_rv.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_geomag_rv.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_gyro_cal.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_gyro_cal.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_mag_cal.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_mag_cal.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_mag_cal_legacy.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_mag_cal_legacy.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_pedometer.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_pedometer.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_rmd.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_rmd.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_rotv.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_rotv.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_smd.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_smd.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_tilt.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_tilt.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_tilt_sw_disabled.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_tilt_sw_disabled.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_tilt_sw_enabled.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_tilt_sw_enabled.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/sns_tilt_to_wake.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/sns_tilt_to_wake.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/tcs3720_0.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/tcs3720_0.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/tcs3720_0_csot_dvt2.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/tcs3720_0_csot_dvt2.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/tcs3720_0_csotc4_always.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/tcs3720_0_csotc4_always.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/tcs3720_0_csotc4_pvt1.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/tcs3720_0_csotc4_pvt1.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/tcs3720_0_csotc6_always.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/tcs3720_0_csotc6_always.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/tcs3720_0_csotc6_pvt1.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/tcs3720_0_csotc6_pvt1.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/tcs3720_0_evt_dvt1.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/tcs3720_0_evt_dvt1.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/tcs3720_0_tianma_always.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/tcs3720_0_tianma_always.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/tcs3720_0_tianma_dvt2.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/tcs3720_0_tianma_dvt2.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/waipio_default_sensors.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/waipio_default_sensors.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/waipio_dynamic_sensors.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/waipio_dynamic_sensors.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/waipio_irq.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/waipio_irq.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/config/waipio_power_0.json:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/config/waipio_power_0.json \
    vendor/motorola/eqs/proprietary/vendor/etc/sensors/sns_reg_config:$(TARGET_COPY_OUT_VENDOR)/etc/sensors/sns_reg_config \
    vendor/motorola/eqs/proprietary/vendor/etc/st21nfc_conf_ds.txt:$(TARGET_COPY_OUT_VENDOR)/etc/st21nfc_conf_ds.txt \
    vendor/motorola/eqs/proprietary/vendor/etc/st21nfc_conf_ss.txt:$(TARGET_COPY_OUT_VENDOR)/etc/st21nfc_conf_ss.txt \
    vendor/motorola/eqs/proprietary/vendor/etc/st54j_conf_ds.txt:$(TARGET_COPY_OUT_VENDOR)/etc/st54j_conf_ds.txt \
    vendor/motorola/eqs/proprietary/vendor/etc/st54j_conf_ss.txt:$(TARGET_COPY_OUT_VENDOR)/etc/st54j_conf_ss.txt \
    vendor/motorola/eqs/proprietary/vendor/etc/st54uicc_conf_ds.txt:$(TARGET_COPY_OUT_VENDOR)/etc/st54uicc_conf_ds.txt \
    vendor/motorola/eqs/proprietary/vendor/etc/st54uicc_conf_ss.txt:$(TARGET_COPY_OUT_VENDOR)/etc/st54uicc_conf_ss.txt \
    vendor/motorola/eqs/proprietary/vendor/etc/tetras/portrait_repair_composite.model:$(TARGET_COPY_OUT_VENDOR)/etc/tetras/portrait_repair_composite.model \
    vendor/motorola/eqs/proprietary/vendor/etc/thermal-engine-eqs.conf:$(TARGET_COPY_OUT_VENDOR)/etc/thermal-engine-eqs.conf \
    vendor/motorola/eqs/proprietary/vendor/etc/thermal-engine.conf:$(TARGET_COPY_OUT_VENDOR)/etc/thermal-engine.conf \
    vendor/motorola/eqs/proprietary/vendor/firmware/cps4035.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/cps4035.bin \
    vendor/motorola/eqs/proprietary/vendor/firmware/csot_goodix_cfg_group.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/csot_goodix_cfg_group.bin \
    vendor/motorola/eqs/proprietary/vendor/firmware/csot_goodix_test_limits_255.csv:$(TARGET_COPY_OUT_VENDOR)/firmware/csot_goodix_test_limits_255.csv \
    vendor/motorola/eqs/proprietary/vendor/firmware/goodix-csot-gt9916-23021720-646772e8-eqs.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/goodix-csot-gt9916-23021720-646772e8-eqs.bin \
    vendor/motorola/eqs/proprietary/vendor/firmware/goodix-tianma-gt9916-23021720-646772e8-eqs.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/goodix-tianma-gt9916-23021720-646772e8-eqs.bin \
    vendor/motorola/eqs/proprietary/vendor/firmware/mot_bu63169.coeff:$(TARGET_COPY_OUT_VENDOR)/firmware/mot_bu63169.coeff \
    vendor/motorola/eqs/proprietary/vendor/firmware/mot_bu63169.prog:$(TARGET_COPY_OUT_VENDOR)/firmware/mot_bu63169.prog \
    vendor/motorola/eqs/proprietary/vendor/firmware/st21nfc_fw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/st21nfc_fw.bin \
    vendor/motorola/eqs/proprietary/vendor/firmware/st21nfc_fw7.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/st21nfc_fw7.bin \
    vendor/motorola/eqs/proprietary/vendor/firmware/st54j_fw.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/st54j_fw.bin \
    vendor/motorola/eqs/proprietary/vendor/firmware/tianma_goodix_cfg_group.bin:$(TARGET_COPY_OUT_VENDOR)/firmware/tianma_goodix_cfg_group.bin \
    vendor/motorola/eqs/proprietary/vendor/firmware/tianma_goodix_test_limits_255.csv:$(TARGET_COPY_OUT_VENDOR)/firmware/tianma_goodix_test_limits_255.csv \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/arcsoft_dc_calibration_u.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/arcsoft_dc_calibration_u.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/com.qti.sensormodule.mot_eqs_imx663_ofilm.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/com.qti.sensormodule.mot_eqs_imx663_ofilm.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/com.qti.sensormodule.mot_eqs_ov60a_qtech.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/com.qti.sensormodule.mot_eqs_ov60a_qtech.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/com.qti.sensormodule.mot_eqs_s5khp1_qtech.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/com.qti.sensormodule.mot_eqs_s5khp1_qtech.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/com.qti.sensormodule.mot_eqs_s5kjn1_qtech.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/com.qti.sensormodule.mot_eqs_s5kjn1_qtech.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/com.qti.tuned.default.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/com.qti.tuned.default.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/com.qti.tuned.mot_eqs_imx663.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/com.qti.tuned.mot_eqs_imx663.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/com.qti.tuned.mot_eqs_ov60a.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/com.qti.tuned.mot_eqs_ov60a.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/com.qti.tuned.mot_eqs_s5khp1.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/com.qti.tuned.mot_eqs_s5khp1.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/com.qti.tuned.mot_eqs_s5kjn1.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/com.qti.tuned.mot_eqs_s5kjn1.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/fdconfigpreview.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/fdconfigpreview.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/fdconfigpreviewlite.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/fdconfigpreviewlite.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/fdconfigvideo.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/fdconfigvideo.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/camera/fdconfigvideolite.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/camera/fdconfigvideolite.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/frontier_arcsoft_portrait_super_night_se_raw.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/frontier_arcsoft_portrait_super_night_se_raw.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/frontier_arcsoft_super_night_raw.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/frontier_arcsoft_super_night_raw.bin \
    vendor/motorola/eqs/proprietary/vendor/lib64/frontier_arcsoft_super_night_se_raw.bin:$(TARGET_COPY_OUT_VENDOR)/lib64/frontier_arcsoft_super_night_se_raw.bin

PRODUCT_PACKAGES += \
    android.hardware.secure_element@1.0-impl-gto \
    android.hardware.secure_element@1.1-impl-gto \
    android.hardware.secure_element@1.2-impl-gto \
    com.mot.eeprom.mot_gt24p128e_imx663_eeprom \
    com.mot.eeprom.mot_gt24p128e_s5khp1_eeprom \
    com.mot.eeprom.mot_gt24p128e_s5kjn1_eeprom \
    com.mot.eeprom.mot_gt24p64e_ov60a_eeprom \
    com.qti.ois.mot_bu63169 \
    com.qti.sensor.mot_imx663 \
    com.qti.sensor.mot_ov60a \
    com.qti.sensor.mot_s5khp1 \
    com.qti.sensor.mot_s5kjn1 \
    com.arcsoft.node.dc_capture \
    com.arcsoft.node.smooth_transition \
    com.arcsoft.node.supernightraw \
    com.bots.node.vendortagwrite \
    com.mot.node.c2d \
    com.mot.node.hdr \
    com.mot.node.scene_detect \
    com.qti.camx.chiiqutils \
    com.qti.eisv2 \
    com.qti.eisv3 \
    com.qti.hvx.addconstant \
    com.qti.hvx.binning \
    com.qti.node.afbfusion \
    com.qti.node.aon \
    com.qti.node.customhwnode \
    com.qti.node.depth \
    com.qti.node.dewarp \
    com.qti.node.dummydepth \
    com.qti.node.dummyrtb \
    com.qti.node.dummysat \
    com.qti.node.eisv2 \
    com.qti.node.eisv3 \
    com.qti.node.fcv \
    com.qti.node.formatconversion \
    com.qti.node.gme \
    com.qti.node.gpu \
    com.qti.node.gyrornn \
    com.qti.node.hdr10pgen \
    com.qti.node.hdr10phist \
    com.qti.node.memcpy \
    com.qti.node.ml \
    com.qti.node.mlinference \
    com.qti.node.muxer \
    com.qti.node.remosaic \
    com.qti.node.stich \
    com.qti.node.swaidenoiser \
    com.qti.node.swbestats \
    com.qti.node.swcac \
    com.qti.node.swec \
    com.qti.node.swfusion \
    com.qti.node.swhme \
    com.qti.node.swlsc \
    com.qti.node.swmctf \
    com.qti.node.swmfnr \
    com.qti.node.swpdpc \
    com.qti.node.swpreprocess \
    com.qti.node.swregistration \
    com.qti.stats.aec \
    com.qti.stats.aecwrapper \
    com.qti.stats.aecxcore \
    com.qti.stats.af \
    com.qti.stats.afd \
    com.qti.stats.afwrapper \
    com.qti.stats.asd \
    com.qti.stats.awb \
    com.qti.stats.awbwrapper \
    com.qti.stats.cnndriver \
    com.qti.stats.haf \
    com.qti.stats.hafoverride \
    com.qti.stats.localhistogram \
    com.qti.stats.pdlib \
    com.qti.stats.pdlibsony \
    com.qti.stats.pdlibwrapper \
    com.qti.stats.statsgenerator \
    com.qti.stats.tracker \
    com.qtistatic.stats.aec \
    com.qtistatic.stats.af \
    com.qtistatic.stats.awb \
    com.qtistatic.stats.pdlib \
    com.vidhance.node.ica \
    com.vidhance.node.processing \
    com.vidhance.stats.aec_dmbr \
    libdepthmapwrapper_secure \
    camx.device@3.2-impl \
    camx.device@3.3-impl \
    camx.device@3.4-ext-impl \
    camx.device@3.4-impl \
    camx.device@3.5-ext-impl \
    camx.device@3.5-impl \
    camx.device@3.6-ext-impl \
    camx.device@3.6-impl \
    camx.device@3.7-impl \
    camx.provider@2.4-external \
    camx.provider@2.4-impl \
    camx.provider@2.4-legacy \
    camx.provider@2.5-external \
    camx.provider@2.5-legacy \
    camx.provider@2.6-legacy \
    camx.provider@2.7-legacy \
    com.qti.chiusecaseselector \
    com.qti.feature2.anchorsync \
    com.qti.feature2.arcrawpro \
    com.qti.feature2.demux \
    com.qti.feature2.derivedoffline \
    com.qti.feature2.frameselect \
    com.qti.feature2.fusion \
    com.qti.feature2.generic \
    com.qti.feature2.gs.cedros \
    com.qti.feature2.gs.fillmore \
    com.qti.feature2.gs.sdm865 \
    com.qti.feature2.gs.sm8350 \
    com.qti.feature2.gs.sm8450 \
    com.qti.feature2.hdr \
    com.qti.feature2.mcreprocrt \
    com.qti.feature2.memcpy \
    com.qti.feature2.mfsr.netrani \
    com.qti.feature2.mfsr.sm8450 \
    com.qti.feature2.mfsr \
    com.qti.feature2.ml.fillmore \
    com.qti.feature2.ml \
    com.qti.feature2.mux \
    com.qti.feature2.qcfa \
    com.qti.feature2.rawhdr \
    com.qti.feature2.realtimeserializer \
    com.qti.feature2.rt \
    com.qti.feature2.rtmcx \
    com.qti.feature2.serializer \
    com.qti.feature2.statsregeneration \
    com.qti.feature2.stub \
    com.qti.feature2.swmf \
    com.qti.qseeaon \
    com.qti.qseeutils \
    com.qti.settings.sm8450 \
    com.qti.stats.common \
    com.qualcomm.mcx.distortionmapper \
    com.qualcomm.mcx.linearmapper \
    com.qualcomm.mcx.policy.mfl \
    com.qualcomm.mcx.policy.xr \
    com.qualcomm.qti.mcx.usecase.extension \
    camera.qcom \
    com.qti.chi.override \
    libAncHumanSegFigureFusion \
    libFace3DTA \
    libFace3D_hlos \
    libPlatformValidatorShared \
    libQ6MSFR_manager_stub \
    libRbsFlow \
    libSNPE \
    libSnpeHtpPrepare \
    libSnpeHtpV68Stub \
    libSnpeHtpV69Stub \
    liba2d_helper \
    libaidenoiser \
    libaidenoiserv2 \
    libancbase_rt_fusion \
    libarcsoft_chi_utils \
    libarcsoft_hdr_detection \
    libarcsoft_high_dynamic_range \
    libarcsoft_mcxmflpolicy \
    libarcsoft_portrait_distortion_correction \
    libarcsoft_portrait_super_night_se_raw \
    libarcsoft_qnnhtp \
    libarcsoft_super_night_raw \
    libarcsoft_super_night_se_raw \
    libarcsoft_triple_sat \
    libarcsoft_triple_zoomtranslator \
    libbitmlengine \
    libbitmlenginev2 \
    libc++_shared \
    libcalculator \
    libcalculator_htp \
    libcamera_nn_stub \
    libcamerapostproc \
    libcamxcommonutils \
    libcamxexternalformatutils \
    libcamxfacialfeatures \
    libcamxfdalgo \
    libcamxfdengine \
    libcamxhwnodecontext \
    libcamxifestriping \
    libcamximageformatutils \
    libcamxqsatalgo \
    libcamxsettingsmanager \
    libcamxstatscore \
    libcamxswispiqmodule \
    libcamxswprocessalgo \
    libcamxtintlessalgo \
    libchilog \
    libcom.qti.chinodeutils \
    libets_teeclient_v3 \
    libeye_tracking_dsp_sample_stub \
    libface3d_dev \
    libfcell \
    libflatbuffers-cpp_vendor \
    libhdr10plus \
    libhme \
    libhta \
    libipebpsstriping \
    libipebpsstriping170 \
    libipebpsstriping480 \
    libjpege \
    libmfGhostDetection \
    libmfec \
    libmmcamera_bestats \
    libmmcamera_cac3 \
    libmmcamera_lscv35 \
    libmmcamera_mfnr \
    libmmcamera_mfnr_t4 \
    libmmcamera_pdpc \
    libmot_afd \
    libmot_chi_desktop_helper \
    libmot_engine_settings \
    libmotcameramodulemonitor \
    libmotoisdataqueue \
    libmpbase \
    libopencv \
    libopencv3a \
    libopestriping \
    libos \
    libportrait_repair_ppl3_ocl \
    libqll \
    libqll10 \
    libqllengine \
    libqshcamera \
    libremosaic_wrapper \
    libremosaiclib_s5khp1 \
    libremosaiclib_s5kjn1 \
    librmsclib1 \
    libruy_vendor \
    libsfeShiftExtrapolation \
    libsnpe_dsp_domains_v2 \
    libsnpe_loader \
    libstnfc-auth \
    libswregistrationalgo \
    libsynx \
    libtextclassifier_hash_vendor \
    libtfestriping \
    libtflite_vendor \
    libthreadutils \
    libtriplecam_optical_zoom_control \
    libtriplecam_video_optical_zoom \
    libubifocus \
    libvidhance \
    nfc_nci.st21nfc.st \
    panel_als_comp_filter_eqs \
    panel_als_comp_filter_eqs_csotc6 \
    panel_als_comp_filter_eqs_tianma \
    vendor.egistec.hardware.fingerprint@4.0 \
    vendor.qti.hardware.camera.aon@1.0-service-impl \
    vendor.qti.hardware.camera.postproc@1.0-service-impl \
    vendor.qti.hardware.fingerprint@1.0 \
    bm2n08 \
    bm2n09 \
    libadsp_jpege_skel \
    libapn_dsp_skel \
    libbitml_nsp_skel \
    libbitml_nsp_v2_skel \
    libcamera_nn_skel \
    libhdr_skel \
    libmctfengine_skel \
    MotoSignatureApp \
    MotCamera3AI \
    HotwordEnrollmentOKGoogleHEXAGON_WIDEBAND \
    HotwordEnrollmentXGoogleHEXAGON_WIDEBAND \
    MotCamera4 \
    MotorolaSettingsProvider \
    com.motorola.motosignature \
    moto-checkin \
    moto-core_services \
    moto-settings \
    com.android.hotwordenrollment.common.util \
    motorola.hardware.sensorext.service.xml \
    STFlashTool \
    android.hardware.biometrics.fingerprint@2.1-service-ets \
    android.hardware.nfc@1.2-service.st \
    android.hardware.secure_element@1.2-service-gto \
    motorola.hardware.sensorext-service \
    vendor.qti.camera.provider@2.7-service_64 \
    motsettings

PRODUCT_PACKAGES += \
    CarrierConfigOverlay
